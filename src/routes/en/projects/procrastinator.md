  <article class="container">
  <h1>Procrastinator</h1>
    <div>
    <p>pseudo-interactive video art / software art</p>

<p>this is work in inter-medium genre, meta-procrastination for people too lazy to do so, ships as .iso image file with auto-procrastinate linux os.
made in: linux, ubuntu, i3wm, python, user-side programs</p>

<p>screenshots</p>

<br/>
<br/>
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/001.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/002.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/003.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/004.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/005.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/006.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/007.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/008.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/009.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/010.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/011.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/011.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/012.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/013.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/014.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/015.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/016.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/017.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/018.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/019.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/020.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/021.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/022.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/023.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/024.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/025.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/026.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/027.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/028.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/029.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/030.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/031.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/032.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/033.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/034.png" alt="alt text" title="image" />
<img src="https://raw.githubusercontent.com/agamurian/agamurian.github.io/master/assets/img/035.png" alt="alt text" title="image" />


  </article>


<style>
  img {
    display:block;
    max-width: 100%;
    max-height: 77vh;
    border-radius: 10px;
    border:none;
    outline:none;
    margin: 15px;
  }

</style>
