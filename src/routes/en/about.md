<script>
import Cubed from '$lib/components/Cubed.svelte'
</script>


<div class="container">

# About me:

### Hi all, my name is Andrew

---
 I am artist, programmer, 3d generalist


#### i use:
- Blender
- Python
- Svelte
- Linux
- js
- sh
 
andalso i can draw in 2d


## Github:
```
https://github.com/agamurian/
```

this website is written by me - AGAMURIAN

<div class="ctrl">
<Cubed/>
</div>

</div>

<style lang="scss"> 
  .ctrl {
    border-radius: 1rem;
    position: relative;
    overflow: hidden;
    width: 100%;
    height: 70vh;
    border: 2px solid black;
  }
  .container {
    canvas {
      border-radius: 1rem;
      width: 100%;
      height: 50vh;
      margin: auto;
    }
  }
</style>
