<!-- Import the component -->
<!--<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>-->

<h2>
  3D Models on web page
</h2>
<h4>
  Google model-viewer, glb
</h4>
<br>
<br>
<!-- Use it like any other HTML element -->
<model-viewer 
style="z-index:9999; width:100%; height: 75vh; background: rgba(76, 175, 80, 0.6);border-radius:20px; padding:10px;"
alt="Neil Armstrong's Spacesuit from the Smithsonian Digitization Programs Office and National Air and Space Museum"
src="/cat.glb"
ar environment-image="/env.hdr"
poster="/cat.webp"
shadow-intensity="1" 
camera-controls touch-action="pan-y">
</model-viewer>
<br>
<br>
<!-- Use it like any other HTML element -->
<model-viewer 
style="z-index:9999; width:100%; height: 75vh; background: rgba(76, 175, 80, 0.6);border-radius:20px; padding:10px;"
alt="Neil Armstrong's Spacesuit from the Smithsonian Digitization Programs Office and National Air and Space Museum"
src="/cat.glb"
ar environment-image="/env.hdr"
poster="/cat.webp"
shadow-intensity="1" 
camera-controls touch-action="pan-y">
</model-viewer>
